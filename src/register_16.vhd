------------------------------------------------------------------------------------------
--
--  \ /   LUNA CORE
--  ( )   REGISTER_16.VHD -- 16bit Register with increase and shifting abilities
--  / \   Copyright (C) 2015 Arman Hajishafieha
--
--    This file is part of LUNA CORE.
--
--    LUNA CORE is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    LUNA CORE is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with LUNA CORE.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity REGISTER_16 is
	port (R16_OUTPUT: inout std_logic_vector(15 downto 0) := (others => '0');
			R16_INPUT: in std_logic_vector (15 downto 0);
			R16_LOAD: in std_logic := '0';
			R16_SHR: in std_logic := '0';
			R16_SHL: in std_logic := '0';
			R16_INC: in std_logic := '0';
			R16_CLK: in std_logic);
end REGISTER_16;

	
architecture Behavioral of REGISTER_16 is

begin
	process(R16_CLK)
	begin
	if R16_CLK'event and R16_CLK = '1' then
			if R16_LOAD = '1' then
				R16_OUTPUT <= R16_INPUT;
			else if R16_INC = '1' then
				R16_OUTPUT <= std_logic_vector(unsigned(R16_OUTPUT) + 1);
			else if R16_SHR = '1' then
				R16_OUTPUT <= std_logic_vector(shift_right(unsigned(R16_OUTPUT), 1));
			else if R16_SHL = '1' then
				R16_OUTPUT <= std_logic_vector(shift_left(unsigned(R16_OUTPUT), 1));
			end if;
			end if;
			end if;
			end if;
		end if;
	end process;

end Behavioral;

