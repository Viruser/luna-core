------------------------------------------------------------------------------------------
--
--  \ /   LUNA CORE
--  ( )   LUNA_TEST.VHD -- Test module used for simulation and debugging
--  / \   Copyright (C) 2015 Arman Hajishafieha                   
--
--    This file is part of LUNA CORE.
--
--    LUNA CORE is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    LUNA CORE is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with LUNA CORE.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use work.CONSTANTS.ALL;

 
ENTITY luna_test IS
END luna_test;
 
ARCHITECTURE behavior OF luna_test IS 
  
    COMPONENT LUNA
    PORT(
         GCLK : IN  std_logic;
         GRX : IN  std_logic;
         GTX : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal GCLK : std_logic := '0';
   signal GRX : std_logic := '0';

 	--Outputs
   signal GTX : std_logic;

   -- Clock period definitions
   constant GCLK_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: LUNA PORT MAP (
          GCLK => GCLK,
          GRX => GRX,
          GTX => GTX
        );

   -- Clock process definitions
   GCLK_process :process
   begin
		GCLK <= '0';
		wait for GCLK_period/2;
		GCLK <= '1';
		wait for GCLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for GCLK_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
