----------------------------------------------------------------------------------
--
--  \ /   LUNA CORE
--  ( )   ROM.VHD -- ROM block that holds first cpu instructions for basic IO
--  / \   Copyright (C) 2015 Arman Hajishafieha
--
--    This file is part of LUNA CORE.
--
--    LUNA CORE is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    LUNA CORE is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with LUNA CORE.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.constants.ALL;

entity ROM_256x16 is
	port(ROM_ADDRESS: in std_logic_vector(7 downto 0);
		  ROM_OUTPUT: out std_logic_vector(15 downto 0) := INSTRUCTION_JMP & "1000000000";
		  ROM_CLK: in std_logic);
end ROM_256x16;

architecture Behavioral of ROM_256x16 is
	type ROM_ARRAY_T is array (0 to 255) of std_logic_vector(15 downto 0);
	constant ROM_DATA: ROM_ARRAY_T := (
0 => X"0720",
1 => X"0010",
2 => X"4320",
3 => X"0001",
4 => X"0432",
5 => X"4B30",
6 => X"00FE",
7 => X"4022",
8 => X"5022",
9 => X"4220",
10 => X"0002",
others => X"0000");





		

begin
	process(ROM_CLK)
		begin
			if ROM_CLK'event and ROM_CLK = '1' then
				ROM_OUTPUT <= ROM_DATA(to_integer(unsigned(ROM_ADDRESS)));
			end if;
	end process;
end Behavioral;

