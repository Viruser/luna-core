------------------------------------------------------------------------------------------
--
--  \ /   LUNA CORE
--  ( )   MEMORY_16384x16.VHD -- Main memory block
--  / \   Copyright (C) 2015 Arman Hajishafieha                  
--
--    This file is part of LUNA CORE.
--
--    LUNA CORE is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    LUNA CORE is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with LUNA CORE.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.constants.ALL;

entity MEMORY_16384x16 is
	port (MEM_OUTPUT: out std_logic_vector(15 downto 0);
			MEM_EXTRAOUT: out std_logic_vector(15 downto 0);
			MEM_ADDRESS: in std_logic_vector (15 downto 0);
			MEM_INPUT: in std_logic_vector (15 downto 0);
			MEM_LOAD: in std_logic := '0';
			MEM_CLK: in std_logic);
end MEMORY_16384x16;

architecture Behavioral of MEMORY_16384x16 is
	type MEM_DATA_ARRAY is array (0 to 16383) of std_logic_vector (15 downto 0);
	shared variable MEM_DATA: MEM_DATA_ARRAY := ((others => "0000000000000000"));

begin
	process(MEM_CLK)
		begin -- this needs debugging
			if MEM_CLK'event and MEM_CLK = '1' then
				if MEM_LOAD = '1' then
					MEM_DATA(to_integer(unsigned(MEM_ADDRESS(13 downto 0)))) := MEM_INPUT;
				end if;
					MEM_OUTPUT <= MEM_DATA(to_integer(unsigned(MEM_ADDRESS(13 downto 0))));
					MEM_EXTRAOUT <= MEM_DATA(to_integer(unsigned(MEM_ADDRESS(13 downto 0))) + 1);
			end if;
	end process;
end Behavioral;
