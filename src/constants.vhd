------------------------------------------------------------------------------------------
--
--  \ /   LUNA CORE
--  ( )   CONSTANTS.VHD -- Constants used to maintain code readability
--  / \   Copyright (C) 2015 Arman Hajishafieha
--
--    This file is part of LUNA CORE.
--
--    LUNA CORE is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    LUNA CORE is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with LUNA CORE.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package CONSTANTS is
	
	constant INSTRUCTION_NOP   : std_logic_vector(5 downto 0) := "000000";
	constant INSTRUCTION_MOV   : std_logic_vector(5 downto 0) := "000001";
	constant INSTRUCTION_JMP   : std_logic_vector(5 downto 0) := "000010";
	constant INSTRUCTION_JZ    : std_logic_vector(5 downto 0) := "000011";
	constant INSTRUCTION_JNZ   : std_logic_vector(5 downto 0) := "000100";
	constant INSTRUCTION_JO    : std_logic_vector(5 downto 0) := "000101";
	constant INSTRUCTION_JNO   : std_logic_vector(5 downto 0) := "000110";
	constant INSTRUCTION_JC    : std_logic_vector(5 downto 0) := "000111";
	constant INSTRUCTION_JNC   : std_logic_vector(5 downto 0) := "001000";
	constant INSTRUCTION_JS    : std_logic_vector(5 downto 0) := "001001";
	constant INSTRUCTION_JNS   : std_logic_vector(5 downto 0) := "001010";
	constant INSTRUCTION_JIR   : std_logic_vector(5 downto 0) := "001011";
	constant INSTRUCTION_JNIR  : std_logic_vector(5 downto 0) := "001100";
	constant INSTRUCTION_JOR   : std_logic_vector(5 downto 0) := "001101";
	constant INSTRUCTION_JNOR  : std_logic_vector(5 downto 0) := "001110";
	constant INSTRUCTION_CALL  : std_logic_vector(5 downto 0) := "001111";
	constant INSTRUCTION_RETN  : std_logic_vector(5 downto 0) := "001111";
	constant INSTRUCTION_ADD   : std_logic_vector(5 downto 0) := "010000";
	constant INSTRUCTION_SUB   : std_logic_vector(5 downto 0) := "010001";
	constant INSTRUCTION_AND   : std_logic_vector(5 downto 0) := "010010";
	constant INSTRUCTION_OR    : std_logic_vector(5 downto 0) := "010011";
	constant INSTRUCTION_XOR   : std_logic_vector(5 downto 0) := "010100";
	constant INSTRUCTION_MUL   : std_logic_vector(5 downto 0) := "010101";
	constant INSTRUCTION_DIV   : std_logic_vector(5 downto 0) := "010110";
	constant INSTRUCTION_SET   : std_logic_vector(5 downto 0) := "011000";
	constant INSTRUCTION_USET  : std_logic_vector(5 downto 0) := "011001";
	constant INSTRUCTION_INC   : std_logic_vector(5 downto 0) := "011010";
	constant INSTRUCTION_SHL   : std_logic_vector(5 downto 0) := "011011";
	constant INSTRUCTION_SHR   : std_logic_vector(5 downto 0) := "011100";
	constant INSTRUCTION_INPUT : std_logic_vector(5 downto 0) := "100000";
	constant INSTRUCTION_OUTPUT: std_logic_vector(5 downto 0) := "100001";

	constant AB_SELECTION_IP : std_logic_vector(2 downto 0) := "001";
	constant AB_SELECTION_GR1: std_logic_vector(2 downto 0) := "010";
	constant AB_SELECTION_GR2: std_logic_vector(2 downto 0) := "011";
	constant AB_SELECTION_GR3: std_logic_vector(2 downto 0) := "100";
	constant AB_SELECTION_GR4: std_logic_vector(2 downto 0) := "101";
	constant AB_SELECTION_EXM: std_logic_vector(2 downto 0) := "110";
	constant AB_SELECTION_MEM     : std_logic_vector(2 downto 0):= "000"; -- used in alu bus only
	constant AB_SELECTION_TWO     : std_logic_vector(2 downto 0):= "111"; -- used in alu bus only	
	constant AB_SELECTION_IRETADDR: std_logic_vector(2 downto 0):= "000"; -- used in address bus only
	constant AB_SELECTION_RETNADDR: std_logic_vector(2 downto 0):= "111"; -- used in address bus only
	
	constant CB_SELECTION_MEM: std_logic_vector(3 downto 0) := "0000";
	constant CB_SELECTION_IP : std_logic_vector(3 downto 0) := "0001";
	constant CB_SELECTION_GR1: std_logic_vector(3 downto 0) := "0010";
	constant CB_SELECTION_GR2: std_logic_vector(3 downto 0) := "0011";
	constant CB_SELECTION_GR3: std_logic_vector(3 downto 0) := "0100";
	constant CB_SELECTION_GR4: std_logic_vector(3 downto 0) := "0101";
	constant CB_SELECTION_EXM : std_logic_vector(3 downto 0) := "0110";
	constant CB_SELECTION_ALU: std_logic_vector(3 downto 0) := "0111";
	constant CB_SELECTION_DATA: std_logic_vector(3 downto 0) := "1001";
	constant CB_SELECTION_TR : std_logic_vector(3 downto 0) := "1010";


	constant ALU_SELECTION_NOP: std_logic_vector(2 downto 0) := "000";
	constant ALU_SELECTION_ADD: std_logic_vector(2 downto 0) := "001";
	constant ALU_SELECTION_SUB: std_logic_vector(2 downto 0) := "010";
	constant ALU_SELECTION_AND: std_logic_vector(2 downto 0) := "011";
	constant ALU_SELECTION_OR : std_logic_vector(2 downto 0) := "100";
	constant ALU_SELECTION_XOR: std_logic_vector(2 downto 0) := "101";
	constant ALU_SELECTION_MUL: std_logic_vector(2 downto 0) := "110";
	constant ALU_SELECTION_DIV: std_logic_vector(2 downto 0) := "111";

	constant FL_SELECTION_ZF: std_logic_vector(2 downto 0) := "000";
	constant FL_SELECTION_SF: std_logic_vector(2 downto 0) := "001";
	constant FL_SELECTION_OF: std_logic_vector(2 downto 0) := "010";
	constant FL_SELECTION_CF: std_logic_vector(2 downto 0) := "011";
	constant FL_SELECTION_IEN: std_logic_vector(2 downto 0):= "100";
	constant FL_SELECTION_CEF: std_logic_vector(2 downto 0):= "101";
	constant FL_SELECTION_IRF: std_logic_vector(2 downto 0):= "110";
	constant FL_SELECTION_ORF: std_logic_vector(2 downto 0):= "111";
	

end CONSTANTS;

package body CONSTANTS is

end CONSTANTS;
