------------------------------------------------------------------------------------------
--
--  \ /   LUNA CORE
--  ( )   CORE.VHD -- Processor Core block
--  / \   Copyright (C) 2015-2016 Arman Hajishafieha
--
--    This file is part of LUNA CORE.
--
--    LUNA CORE is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    LUNA CORE is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with LUNA CORE.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.CONSTANTS.ALL;


entity CORE is
	port (CLK: in std_logic;
			MEM_OUTPUT: in std_logic_vector(15 downto 0);
			MEM_EXTRAOUT: in std_logic_vector(15 downto 0);
			MEM_LOAD: out std_logic := '0';
			MEM_INPUT: out std_logic_vector(15 downto 0) := (others => '0');
			MEM_ADDRESS: out std_logic_vector(15 downto 0) := (others => '0');
			DATA_OUTPUT: out std_logic_vector(7 downto 0) := (others => '0');
			DATA_INPUT: in std_logic_vector(7 downto 0);
			DATA_INPUT_READY: in std_logic;
			DATA_OUTPUT_READY: in std_logic;
			DATA_OUTPUT_ENABLE: out std_logic := '0';
			CEF_INPUT: in std_logic);
end CORE;

architecture Behavioral of CORE is
component REGISTER_16 is
	port (R16_OUTPUT: inout std_logic_vector(15 downto 0);
			R16_INPUT: in std_logic_vector (15 downto 0);
			R16_LOAD: in std_logic := '0';
			R16_SHR: in std_logic := '0';
			R16_SHL: in std_logic := '0';
			R16_INC: in std_logic := '0';
			R16_CLK: in std_logic);
end component;

component ARITHMETICLOGICALUNIT is
	port(ALU_INPUT_1: in std_logic_vector(15 downto 0);
		  ALU_INPUT_2: in std_logic_vector(15 downto 0);
		  ALU_SELECT:  in std_logic_vector(2 downto 0);
		  ALU_OUTPUT:  inout std_logic_vector(15 downto 0);
		  ALU_CF: inout std_logic;
		  ALU_OF: inout std_logic;
		  ALU_ZF: inout std_logic;
		  ALU_SF: inout std_logic);
end component;

component COMMON_BUS is
	port(CB_INPUT_IP: in std_logic_vector(15 downto 0);
		  CB_INPUT_GR1: in std_logic_vector(15 downto 0);
		  CB_INPUT_GR2: in std_logic_vector(15 downto 0);
		  CB_INPUT_GR3: in std_logic_vector(15 downto 0);
		  CB_INPUT_GR4: in std_logic_vector(15 downto 0);
		  CB_INPUT_DATA: in std_logic_vector(7 downto 0);
		  CB_INPUT_EXM: in std_logic_vector(15 downto 0);
		  CB_INPUT_TR: in std_logic_vector(15 downto 0);
		  CB_INPUT_MEM: in std_logic_vector(15 downto 0);
		  CB_INPUT_ALU: in std_logic_vector(15 downto 0);
		  CB_SELECT: in std_logic_vector(3 downto 0);
		  CB_OUTPUT: out std_logic_vector(15 downto 0));
end component;

component ADDRESS_BUS is
	port(AB_INPUT_GR1: in std_logic_vector(15 downto 0);
	     AB_INPUT_GR2: in std_logic_vector(15 downto 0);
		  AB_INPUT_GR3: in std_logic_vector(15 downto 0);
		  AB_INPUT_GR4: in std_logic_vector(15 downto 0);
		  AB_INPUT_IP: in std_logic_vector(15 downto 0);
		  AB_INPUT_EXM: in std_logic_vector(15 downto 0);
		  AB_SELECT: in std_logic_vector(2 downto 0);
		  AB_OUTPUT: out std_logic_vector(15 downto 0));
end component;

component ALU_BUS is
	port(ALB_INPUT_GR1: in std_logic_vector(15 downto 0);
	     ALB_INPUT_GR2: in std_logic_vector(15 downto 0);
		  ALB_INPUT_GR3: in std_logic_vector(15 downto 0);
		  ALB_INPUT_GR4: in std_logic_vector(15 downto 0);
		  ALB_INPUT_IP: in std_logic_vector(15 downto 0);
		  ALB_INPUT_EXM: in std_logic_vector(15 downto 0);
		  ALB_INPUT_MEM: in std_logic_vector(15 downto 0);
		  ALB_SELECT: in std_logic_vector(2 downto 0);
		  ALB_OUTPUT: out std_logic_vector(15 downto 0));
end component;

component CPUFLAGS is
	port(CF_INPUT: in std_logic;
		  OF_INPUT: in std_logic;
		  ZF_INPUT: in std_logic;
		  SF_INPUT: in std_logic;
		  CEF_INPUT:in std_logic;
		  IEN_INPUT:in std_logic;
		  IRF_INPUT:in std_logic;
		  ORF_INPUT:in std_logic;
		  
		  CF_OUTPUT: out std_logic;
		  OF_OUTPUT: out std_logic;
		  ZF_OUTPUT: out std_logic;
		  SF_OUTPUT: out std_logic;
		  CEF_OUTPUT: out std_logic;
		  IEN_OUTPUT: out std_logic;
		  IRF_OUTPUT: out std_logic;
		  ORF_OUTPUT: out std_logic;
		  
		  FL_LOAD: in std_logic;
		  FL_CLK: in std_logic);
end component;

signal IR_OUTPUT, IP_OUTPUT,
		 GR1_OUTPUT, GR2_OUTPUT, GR3_OUTPUT, GR4_OUTPUT: std_logic_vector(15 downto 0);
signal CB_OUTPUT: std_logic_vector(15 downto 0);
signal IR_LOAD, IP_LOAD, GR1_LOAD, GR2_LOAD, GR3_LOAD, GR4_LOAD: std_logic := '0';
signal IP_INC, GR1_INC, GR2_INC, GR3_INC, GR4_INC: std_logic := '0';
signal GR1_SHL, GR1_SHR, GR2_SHL, GR2_SHR, GR3_SHL, GR3_SHR, GR4_SHL, GR4_SHR: std_logic := '0';

signal CB_SELECT: std_logic_vector(3 downto 0) := (others => '0');
signal AB_SELECT: std_logic_vector(2 downto 0) := (others => '0');
signal ALU_SELECT, ALB1_SELECT, ALB2_SELECT: std_logic_vector(2 downto 0) := (others => '0');
signal ALU_INPUT_1, ALU_INPUT_2, ALU_OUTPUT: std_logic_vector(15 downto 0) := (others => '0');
signal ALU_CF, ALU_OF, ALU_ZF, ALU_SF: std_logic;
signal CF_OUTPUT, OF_OUTPUT, ZF_OUTPUT, SF_OUTPUT, CEF_OUTPUT, IEN_OUTPUT, IRF_OUTPUT, ORF_OUTPUT: std_logic;
signal CF_INPUT, OF_INPUT, ZF_INPUT, SF_INPUT, IEN_INPUT, IRF_INPUT: std_logic := '0';
signal DATA_REGISTER: std_logic_vector(7 downto 0) := (others => '0');
signal TR: std_logic_vector(15 downto 0) := (others => '0');
signal STEPCOUNTER: integer := 0;

begin
										  
	CB: COMMON_BUS port map(CB_INPUT_IP => IP_OUTPUT, CB_INPUT_GR1 => GR1_OUTPUT, CB_INPUT_GR2 => GR2_OUTPUT,
									CB_INPUT_GR3 => GR3_OUTPUT, CB_INPUT_GR4 => GR4_OUTPUT, CB_INPUT_MEM => MEM_OUTPUT, CB_INPUT_ALU => ALU_OUTPUT,
									CB_INPUT_DATA => DATA_REGISTER, CB_INPUT_EXM => MEM_EXTRAOUT, CB_INPUT_TR => TR,
									CB_SELECT => CB_SELECT, CB_OUTPUT => CB_OUTPUT);
	AB: ADDRESS_BUS port map(AB_INPUT_GR1 => GR1_OUTPUT, AB_INPUT_GR2 => GR2_OUTPUT, AB_INPUT_GR3 => GR3_OUTPUT, 
									 AB_INPUT_GR4 => GR4_OUTPUT, AB_INPUT_IP => IP_OUTPUT, AB_INPUT_EXM => MEM_EXTRAOUT,
									 AB_SELECT => AB_SELECT, AB_OUTPUT => MEM_ADDRESS);

	ALB1: ALU_BUS port map(ALB_INPUT_GR1 => GR1_OUTPUT, ALB_INPUT_GR2 => GR2_OUTPUT, ALB_INPUT_GR3 => GR3_OUTPUT, 
								  ALB_INPUT_GR4 => GR4_OUTPUT, ALB_INPUT_IP => IP_OUTPUT, ALB_INPUT_EXM => MEM_EXTRAOUT,
								  ALB_INPUT_MEM => MEM_OUTPUT, ALB_SELECT => ALB1_SELECT, ALB_OUTPUT => ALU_INPUT_1);
	ALB2: ALU_BUS port map(ALB_INPUT_GR1 => GR1_OUTPUT, ALB_INPUT_GR2 => GR2_OUTPUT, ALB_INPUT_GR3 => GR3_OUTPUT, 
								  ALB_INPUT_GR4 => GR4_OUTPUT, ALB_INPUT_IP => IP_OUTPUT, ALB_INPUT_EXM => MEM_EXTRAOUT,
								  ALB_INPUT_MEM => MEM_OUTPUT, ALB_SELECT => ALB2_SELECT, ALB_OUTPUT => ALU_INPUT_2);

	IR: REGISTER_16 port map(R16_OUTPUT => IR_OUTPUT, R16_INPUT => CB_OUTPUT, R16_SHR => '0', R16_SHL => '0',
									 R16_INC => '0', R16_LOAD => IR_LOAD, R16_CLK => CLK);
	IP: REGISTER_16 port map(R16_OUTPUT => IP_OUTPUT, R16_INPUT => CB_OUTPUT, R16_SHR => '0', R16_SHL => '0',
									 R16_LOAD => IP_LOAD, R16_CLK => CLK, R16_INC => IP_INC);
	GR1: REGISTER_16 port map(R16_OUTPUT => GR1_OUTPUT, R16_INPUT => CB_OUTPUT, R16_SHR => GR1_SHR, R16_SHL =>GR1_SHL,
									 R16_LOAD => GR1_LOAD, R16_CLK => CLK, R16_INC => GR1_INC);
	GR2: REGISTER_16 port map(R16_OUTPUT => GR2_OUTPUT, R16_INPUT => CB_OUTPUT, R16_SHR => GR2_SHR, R16_SHL =>GR2_SHL,
									 R16_LOAD => GR2_LOAD, R16_CLK => CLK, R16_INC => GR2_INC);
	GR3: REGISTER_16 port map(R16_OUTPUT => GR3_OUTPUT, R16_INPUT => CB_OUTPUT, R16_SHR => GR3_SHR, R16_SHL =>GR3_SHL,
									 R16_LOAD => GR3_LOAD, R16_CLK => CLK, R16_INC => GR3_INC);
	GR4: REGISTER_16 port map(R16_OUTPUT => GR4_OUTPUT, R16_INPUT => CB_OUTPUT, R16_SHR => GR4_SHR, R16_SHL =>GR4_SHL,
									 R16_LOAD => GR4_LOAD, R16_CLK => CLK, R16_INC => GR4_INC);

	FL: CPUFLAGS port map (CF_INPUT => CF_INPUT, OF_INPUT => OF_INPUT, ZF_INPUT => ZF_INPUT,
								 SF_INPUT => SF_INPUT, CEF_INPUT => CEF_INPUT, IEN_INPUT => IEN_INPUT,
								 IRF_INPUT => IRF_INPUT, ORF_INPUT => DATA_OUTPUT_READY,
								 CF_OUTPUT => CF_OUTPUT, OF_OUTPUT => OF_OUTPUT, ZF_OUTPUT => ZF_OUTPUT,
								 SF_OUTPUT => SF_OUTPUT, CEF_OUTPUT => CEF_OUTPUT, IEN_OUTPUT => IEN_OUTPUT,
								 IRF_OUTPUT => IRF_OUTPUT, ORF_OUTPUT => ORF_OUTPUT,
								 FL_LOAD => '1', FL_CLK => CLK);

	ALU: ARITHMETICLOGICALUNIT port map(ALU_INPUT_1 => ALU_INPUT_1, ALU_INPUT_2 => ALU_INPUT_2, ALU_OUTPUT => ALU_OUTPUT,
							ALU_SELECT => ALU_SELECT, ALU_CF => ALU_CF, ALU_OF => ALU_OF, ALU_ZF => ALU_ZF, ALU_SF => ALU_SF);
	
	MEM_INPUT <= CB_OUTPUT;
	process(CLK)
		begin
			if CLK'event and CLK = '1'  and CEF_OUTPUT = '1' then
			--===========================
			-- clean up.
			IP_LOAD <= '0';
			IR_LOAD <= '0';
			GR1_LOAD <= '0';
			GR2_LOAD <= '0';
			GR3_LOAD <= '0';
			GR4_LOAD <= '0';
			MEM_LOAD <= '0';
			IP_INC <= '0';
			GR1_SHR <= '0';
			GR1_SHL <= '0';
			GR1_INC <= '0';
			GR2_SHR <= '0';
			GR2_SHL <= '0';
			GR2_INC <= '0';
			GR3_SHR <= '0';
			GR3_SHL <= '0';
			GR3_INC <= '0';
			GR4_SHR <= '0';
			GR4_SHL <= '0';
			GR4_INC <= '0';
			if DATA_REGISTER /= DATA_INPUT and DATA_INPUT_READY = '1' then
				DATA_REGISTER <= DATA_INPUT;
				IRF_INPUT <= '1';
			else
				IRF_INPUT <= IRF_OUTPUT;
			end if;
			if ORF_OUTPUT = '0' then
				DATA_OUTPUT_ENABLE <= '0';
			end if;
			--===========================
				case STEPCOUNTER is
					when 0 =>
						if IEN_OUTPUT = '1' and IRF_OUTPUT = '1' then
							--save current IP
							AB_SELECT <= AB_SELECTION_IRETADDR; 
							CB_SELECT <= CB_SELECTION_IP;
							MEM_LOAD <= '1';
							IEN_INPUT <= '0';
							STEPCOUNTER <= 3; 
						else
							CF_INPUT <= CF_OUTPUT; -- make sure flags aren't tied to ALU anymore
							ZF_INPUT <= ZF_OUTPUT;
							OF_INPUT <= OF_OUTPUT;
							SF_INPUT <= SF_OUTPUT;
							IEN_INPUT <= IEN_OUTPUT;
							-- TODO: Some commands finish in 1 clock pulse.
							-- Detect these and skip a clock
							case CB_OUTPUT(15 downto 10) is
							---------------MOV--------------
								when INSTRUCTION_MOV =>
									case CB_OUTPUT(9 downto 8) is
										when "00" => -- R<-R
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											CB_SELECT <= CB_OUTPUT(3 downto 0);
										when "01" => -- R<-M
											AB_SELECT <= AB_SELECTION_EXM; -- load the memory addres found in the second part of instruction
											IP_INC <= '1'; --32bit instruction
											-- now wait for ram to be ready and continue in next step
										when "10" => -- M<-R
											CB_SELECT <= CB_OUTPUT(7 downto 4);
											AB_SELECT <= AB_SELECTION_EXM;
											MEM_LOAD <= '1';
											IP_INC <= '1'; --32bit instruction
										when "11" => -- R<-A
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											CB_SELECT <= CB_SELECTION_EXM;
											IP_INC <= '1'; --32bit instruction
										when others => NULL;
									end case;
							-------------MOV-----------------
							-------------JUMPS-----------------
								when INSTRUCTION_JMP =>
									case CB_OUTPUT(9 downto 8) is
										when "00" => -- address in register
											CB_SELECT <= CB_OUTPUT(7 downto 4);
											IP_LOAD <= '1';
											-- wait for ram to be ready and continue in next step
										when "01" => -- address in memory
											AB_SELECT <= AB_SELECTION_EXM;
											CB_SELECT <= CB_SELECTION_MEM;
											-- wait for ram to be ready and continue in next step
										when "10" => -- absolute address
											CB_SELECT <= CB_SELECTION_EXM;
											AB_SELECT <= AB_SELECTION_EXM;
											IP_LOAD <= '1';
										when "11" => -- Relative address
											ALB1_SELECT <= AB_SELECTION_EXM;
											ALB2_SELECT <= AB_SELECTION_IP;
											ALU_SELECT <= ALU_SELECTION_ADD;
											CB_SELECT <= CB_SELECTION_ALU;
											IP_LOAD <= '1';
										when others => NULL;
									end case;
								when INSTRUCTION_JZ =>
									if ZF_OUTPUT = '1' then
										case CB_OUTPUT(9 downto 8) is
											when "00" => -- address in register
												CB_SELECT <= CB_OUTPUT(7 downto 4);
												IP_LOAD <= '1';
												-- wait for ram to be ready and continue in next step
											when "01" => -- address in memory
												AB_SELECT <= AB_SELECTION_EXM;
												CB_SELECT <= CB_SELECTION_MEM;
												-- wait for ram to be ready and continue in next step
											when "10" => -- absolute address
												CB_SELECT <= CB_SELECTION_EXM;
												IP_LOAD <= '1';
											when "11" =>
												ALB1_SELECT <= AB_SELECTION_EXM;
												ALB2_SELECT <= AB_SELECTION_IP;
												ALU_SELECT <= ALU_SELECTION_ADD;
												CB_SELECT <= CB_SELECTION_ALU;
												--wait for ALU to be ready and continue in next step
											when others => NULL;
										end case;
									end if;
									if ZF_OUTPUT = '0' and CB_OUTPUT(9 downto 8) /= "00" then IP_INC <= '1'; end if;--32bit instruction
								when INSTRUCTION_JNZ =>
									if ZF_OUTPUT = '0' then
										case CB_OUTPUT(9 downto 8) is
											when "00" => -- address in register
												CB_SELECT <= CB_OUTPUT(7 downto 4);
												IP_LOAD <= '1';
												-- wait for ram to be ready and continue in next step
											when "01" => -- address in memory
												AB_SELECT <= AB_SELECTION_EXM;
												CB_SELECT <= CB_SELECTION_MEM;
												-- wait for ram to be ready and continue in next step
											when "10" => -- absolute address
												CB_SELECT <= CB_SELECTION_EXM;
												IP_LOAD <= '1';
											when "11" => --relative address
												ALB1_SELECT <= AB_SELECTION_EXM;
												ALB2_SELECT <= AB_SELECTION_IP;
												ALU_SELECT <= ALU_SELECTION_ADD;
												CB_SELECT <= CB_SELECTION_ALU;
												--wait for ALU to be ready and continue in next step
											when others => NULL;
										end case;
									end if;
									if ZF_OUTPUT = '1' and CB_OUTPUT(9 downto 8) /= "00" then IP_INC <= '1'; end if;--32bit instruction
								when INSTRUCTION_JC =>
									if CF_OUTPUT = '1' then
										case CB_OUTPUT(9 downto 8) is
											when "00" => -- address in register
												CB_SELECT <= CB_OUTPUT(7 downto 4);
												IP_LOAD <= '1';
												-- wait for ram to be ready and continue in next step
											when "01" => -- address in memory
												AB_SELECT <= AB_SELECTION_EXM;
												CB_SELECT <= CB_SELECTION_MEM;
												-- wait for ram to be ready and continue in next step
											when "10" => -- absolute address
												CB_SELECT <= CB_SELECTION_EXM;
												IP_LOAD <= '1';
											when "11" => --relative address
												ALB1_SELECT <= AB_SELECTION_EXM;
												ALB2_SELECT <= AB_SELECTION_IP;
												ALU_SELECT <= ALU_SELECTION_ADD;
												CB_SELECT <= CB_SELECTION_ALU;
												--wait for ALU to be ready and continue in next step
											when others => NULL;
										end case;
									end if;
									if CF_OUTPUT = '0' and CB_OUTPUT(9 downto 8) /= "00" then IP_INC <= '1'; end if;--32bit instruction
								when INSTRUCTION_JNC =>
									if CF_OUTPUT = '0' then
										case CB_OUTPUT(9 downto 8) is
											when "00" => -- address in register
												CB_SELECT <= CB_OUTPUT(7 downto 4);
												IP_LOAD <= '1';
												-- wait for ram to be ready and continue in next step
											when "01" => -- address in memory
												AB_SELECT <= AB_SELECTION_EXM;
												CB_SELECT <= CB_SELECTION_MEM;
												-- wait for ram to be ready and continue in next step
											when "10" => -- absolute address
												CB_SELECT <= CB_SELECTION_EXM;
												IP_LOAD <= '1';
											when "11" => --relative address
												ALB1_SELECT <= AB_SELECTION_EXM;
												ALB2_SELECT <= AB_SELECTION_IP;
												ALU_SELECT <= ALU_SELECTION_ADD;
												CB_SELECT <= CB_SELECTION_ALU;
												--wait for ALU to be ready and continue in next step
											when others => NULL;
										end case;
									end if;
									if CF_OUTPUT = '1' and CB_OUTPUT(9 downto 8) /= "00" then IP_INC <= '1'; end if;--32bit instruction
								when INSTRUCTION_JO =>
									if OF_OUTPUT = '1' then
										case CB_OUTPUT(9 downto 8) is
											when "00" => -- address in register
												CB_SELECT <= CB_OUTPUT(7 downto 4);
												IP_LOAD <= '1';
												-- wait for ram to be ready and continue in next step
											when "01" => -- address in memory
												AB_SELECT <= AB_SELECTION_EXM;
												CB_SELECT <= CB_SELECTION_MEM;
												-- wait for ram to be ready and continue in next step
											when "10" => -- absolute address
												CB_SELECT <= CB_SELECTION_EXM;
												IP_LOAD <= '1';
											when "11" => --relative address
												ALB1_SELECT <= AB_SELECTION_EXM;
												ALB2_SELECT <= AB_SELECTION_IP;
												ALU_SELECT <= ALU_SELECTION_ADD;
												CB_SELECT <= CB_SELECTION_ALU;
												--wait for ALU to be ready and continue in next step
											when others => NULL;
										end case;
									end if;
									if OF_OUTPUT = '0' and CB_OUTPUT(9 downto 8) /= "00" then IP_INC <= '1'; end if;--32bit instruction
								when INSTRUCTION_JNO =>
									if OF_OUTPUT = '0' then
										case CB_OUTPUT(9 downto 8) is
											when "00" => -- address in register
												CB_SELECT <= CB_OUTPUT(7 downto 4);
												IP_LOAD <= '1';
												-- wait for ram to be ready and continue in next step
											when "01" => -- address in memory
												AB_SELECT <= AB_SELECTION_EXM;
												CB_SELECT <= CB_SELECTION_MEM;
												-- wait for ram to be ready and continue in next step
											when "10" => -- absolute address
												CB_SELECT <= CB_SELECTION_EXM;
												IP_LOAD <= '1';
											when "11" => --relative address
												ALB1_SELECT <= AB_SELECTION_EXM;
												ALB2_SELECT <= AB_SELECTION_IP;
												ALU_SELECT <= ALU_SELECTION_ADD;
												CB_SELECT <= CB_SELECTION_ALU;
												--wait for ALU to be ready and continue in next step
											when others => NULL;
										end case;
									end if;
									if OF_OUTPUT = '1' and CB_OUTPUT(9 downto 8) /= "00" then IP_INC <= '1'; end if;--32bit instruction
								when INSTRUCTION_JS =>
									if SF_OUTPUT = '1' then
										case CB_OUTPUT(9 downto 8) is
											when "00" => -- address in register
												CB_SELECT <= CB_OUTPUT(7 downto 4);
												IP_LOAD <= '1';
												-- wait for ram to be ready and continue in next step
											when "01" => -- address in memory
												AB_SELECT <= AB_SELECTION_EXM;
												CB_SELECT <= CB_SELECTION_MEM;
												-- wait for ram to be ready and continue in next step
											when "10" => -- absolute address
												CB_SELECT <= CB_SELECTION_EXM;
												IP_LOAD <= '1';
											when "11" => --relative address
												ALB1_SELECT <= AB_SELECTION_EXM;
												ALB2_SELECT <= AB_SELECTION_IP;
												ALU_SELECT <= ALU_SELECTION_ADD;
												CB_SELECT <= CB_SELECTION_ALU;
												--wait for ALU to be ready and continue in next step
											when others => NULL;
										end case;
									end if;
									if SF_OUTPUT = '0' and CB_OUTPUT(9 downto 8) /= "00" then IP_INC <= '1'; end if;--32bit instruction
								when INSTRUCTION_JNS =>
									if SF_OUTPUT = '0' then
										case CB_OUTPUT(9 downto 8) is
											when "00" => -- address in register
												CB_SELECT <= CB_OUTPUT(7 downto 4);
												IP_LOAD <= '1';
												-- wait for ram to be ready and continue in next step
											when "01" => -- address in memory
												AB_SELECT <= AB_SELECTION_EXM;
												CB_SELECT <= CB_SELECTION_MEM;
												-- wait for ram to be ready and continue in next step
											when "10" => -- absolute address
												CB_SELECT <= CB_SELECTION_EXM;
												IP_LOAD <= '1';
											when "11" => --relative address
												ALB1_SELECT <= AB_SELECTION_EXM;
												ALB2_SELECT <= AB_SELECTION_IP;
												ALU_SELECT <= ALU_SELECTION_ADD;
												CB_SELECT <= CB_SELECTION_ALU;
												--wait for ALU to be ready and continue in next step
											when others => NULL;
										end case;
									end if;
									if SF_OUTPUT = '1' and CB_OUTPUT(9 downto 8) /= "00" then IP_INC <= '1'; end if;--32bit instruction
								when INSTRUCTION_JIR =>
									if IRF_OUTPUT = '1' then
										case CB_OUTPUT(9 downto 8) is
											when "00" => -- address in register
												CB_SELECT <= CB_OUTPUT(7 downto 4);
												IP_LOAD <= '1';
												-- wait for ram to be ready and continue in next step
											when "01" => -- address in memory
												AB_SELECT <= AB_SELECTION_EXM;
												CB_SELECT <= CB_SELECTION_MEM;
												-- wait for ram to be ready and continue in next step
											when "10" => -- absolute address
												CB_SELECT <= CB_SELECTION_EXM;
												IP_LOAD <= '1';
											when "11" => --relative address
												ALB1_SELECT <= AB_SELECTION_EXM;
												ALB2_SELECT <= AB_SELECTION_IP;
												ALU_SELECT <= ALU_SELECTION_ADD;
												CB_SELECT <= CB_SELECTION_ALU;
												--wait for ALU to be ready and continue in next step
											when others => NULL;
										end case;
									end if;
									if IRF_OUTPUT = '0' and CB_OUTPUT(9 downto 8) /= "00" then IP_INC <= '1'; end if;--32bit instruction
								when INSTRUCTION_JNIR =>
									if IRF_OUTPUT = '0' then
										case CB_OUTPUT(9 downto 8) is
											when "00" => -- address in register
												CB_SELECT <= CB_OUTPUT(7 downto 4);
												IP_LOAD <= '1';
												-- wait for ram to be ready and continue in next step
											when "01" => -- address in memory
												AB_SELECT <= AB_SELECTION_EXM;
												CB_SELECT <= CB_SELECTION_MEM;
												-- wait for ram to be ready and continue in next step
											when "10" => -- absolute address
												CB_SELECT <= CB_SELECTION_EXM;
												IP_LOAD <= '1';
											when "11" => --relative address
												ALB1_SELECT <= AB_SELECTION_EXM;
												ALB2_SELECT <= AB_SELECTION_IP;
												ALU_SELECT <= ALU_SELECTION_ADD;
												CB_SELECT <= CB_SELECTION_ALU;
												--wait for ALU to be ready and continue in next step
											when others => NULL;
										end case;
									end if;
									if IRF_OUTPUT = '1' and CB_OUTPUT(9 downto 8) /= "00" then IP_INC <= '1'; end if;--32bit instruction
								when INSTRUCTION_JOR =>
									if ORF_OUTPUT = '1' then
										case CB_OUTPUT(9 downto 8) is
											when "00" => -- address in register
												CB_SELECT <= CB_OUTPUT(7 downto 4);
												IP_LOAD <= '1';
												-- wait for ram to be ready and continue in next step
											when "01" => -- address in memory
												AB_SELECT <= AB_SELECTION_EXM;
												CB_SELECT <= CB_SELECTION_MEM;
												-- wait for ram to be ready and continue in next step
											when "10" => -- absolute address
												CB_SELECT <= CB_SELECTION_EXM;
												IP_LOAD <= '1';
											when "11" => --relative address
												ALB1_SELECT <= AB_SELECTION_EXM;
												ALB2_SELECT <= AB_SELECTION_IP;
												ALU_SELECT <= ALU_SELECTION_ADD;
												CB_SELECT <= CB_SELECTION_ALU;
												--wait for ALU to be ready and continue in next step
											when others => NULL;
										end case;
									end if;
									if ORF_OUTPUT = '0' and CB_OUTPUT(9 downto 8) /= "00" then IP_INC <= '1'; end if;--32bit instruction
								when INSTRUCTION_JNOR =>
									if ORF_OUTPUT = '0' then
										case CB_OUTPUT(9 downto 8) is
											when "00" => -- address in register
												CB_SELECT <= CB_OUTPUT(7 downto 4);
												IP_LOAD <= '1';
												-- wait for ram to be ready and continue in next step
											when "01" => -- address in memory
												AB_SELECT <= AB_SELECTION_EXM;
												CB_SELECT <= CB_SELECTION_MEM;
												-- wait for ram to be ready and continue in next step
											when "10" => -- absolute address
												CB_SELECT <= CB_SELECTION_EXM;
												IP_LOAD <= '1';
											when "11" => --relative address
												ALB1_SELECT <= AB_SELECTION_EXM;
												ALB2_SELECT <= AB_SELECTION_IP;
												ALU_SELECT <= ALU_SELECTION_ADD;
												CB_SELECT <= CB_SELECTION_ALU;
												--wait for ALU to be ready and continue in next step
											when others => NULL;
										end case;
									end if;
									if ORF_OUTPUT = '1' and CB_OUTPUT(9 downto 8) /= "00" then IP_INC <= '1'; end if;--32bit instruction
							-------------JUMPS-----------------
							-------------CALL-----------------
								-- CALL FUNCTION IS BROKEN DUE TO IT REQUIRING MORE THAN 2 
								-- PULSES TO COMPLETE THE INSTRUCTION
	--							when INSTRUCTION_CALL =>
	--								case CB_OUTPUT(9 downto 8) is
	--									when "00" => -- address in register
	--										TR <= IP_OUTPUT; -- hold IP temprarily to save into mem in next clock
	--										CB_SELECT <= CB_OUTPUT(7 downto 4);
	--										
	--										IP_LOAD <= '1';
	--										--wait for IP to update and use CB to update memory in next clock
	--									when "01" => -- address in mem
	--										TR <= IP_OUTPUT;
	--										AB_SELECT <= AB_SELECTION_EXM;
	--										CB_SELECT <= CB_SELECTION_MEM;
	--										IP_LOAD <= '1';
	--										--wait for IP to update and use CB to update memory in next clock
	--									when "10" => -- absolute address
	--										TR <= IP_OUTPUT;
	--										CB_SELECT <= CB_SELECTION_EXM;
	--										IP_LOAD <= '1';
	--										--wait for IP to update and use CB to update memory in next clock
	--									when "11" => --INSTRUCTION_RETN
	--										AB_SELECT <= AB_SELECTION_RETNADDR;
	--										CB_SELECT <= CB_SELECTION_MEM;
	--										IP_LOAD <= '1';
	--										-- IP needs to be increased by 2 in next clock
	--									when others => NULL;
	--								end case;
							-------------CALL-----------------
							-------------ALU-FUNCTIONS--------
								when INSTRUCTION_ADD =>
									case CB_OUTPUT(9 downto 8) is
										when "00" => -- R <= R
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= CB_OUTPUT(2 downto 0);
											ALU_SELECT <= ALU_SELECTION_ADD;
											CB_SELECT <= CB_SELECTION_ALU;
											CF_INPUT <= ALU_CF;
											SF_INPUT <= ALU_SF;
											OF_INPUT <= ALU_OF;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
										when "01" => -- R <= M
											AB_SELECT <= AB_SELECTION_EXM;
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= AB_SELECTION_MEM;
											ALU_SELECT <= ALU_SELECTION_ADD;
											CB_SELECT <= CB_SELECTION_ALU;
											CF_INPUT <= ALU_CF;
											SF_INPUT <= ALU_SF;
											OF_INPUT <= ALU_OF;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											IP_INC <= '1'; --32bit instruction
											-- wait for memory to be ready and continue in next step
										when "10" => -- M <= R
											AB_SELECT <= AB_SELECTION_EXM;
											ALB2_SELECT <= CB_OUTPUT(6 downto 4);
											ALB1_SELECT <= AB_SELECTION_MEM;
											ALU_SELECT <= ALU_SELECTION_ADD;
											AB_SELECT <= AB_SELECTION_EXM;
											CB_SELECT <= CB_SELECTION_ALU;
											CF_INPUT <= ALU_CF;
											SF_INPUT <= ALU_SF;
											OF_INPUT <= ALU_OF;
											ZF_INPUT <= ALU_ZF;
											IP_INC <= '1'; --32bit instruction
											MEM_LOAD <= '1';
										when "11" => -- R <= A
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= AB_SELECTION_EXM;
											ALU_SELECT <= ALU_SELECTION_ADD;
											CB_SELECT <= CB_SELECTION_ALU;
											CF_INPUT <= ALU_CF;
											SF_INPUT <= ALU_SF;
											OF_INPUT <= ALU_OF;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											IP_INC <= '1'; --32bit instruction
											when others => NULL;
									end case;
								when INSTRUCTION_SUB =>
									case CB_OUTPUT(9 downto 8) is
										when "00" => -- R <= R
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= CB_OUTPUT(2 downto 0);
											ALU_SELECT <= ALU_SELECTION_SUB;
											CB_SELECT <= CB_SELECTION_ALU;
											CF_INPUT <= ALU_CF;
											SF_INPUT <= ALU_SF;
											OF_INPUT <= ALU_OF;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
										when "01" => -- R <= M
											AB_SELECT <= AB_SELECTION_EXM;
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= AB_SELECTION_MEM;
											ALU_SELECT <= ALU_SELECTION_SUB;
											CB_SELECT <= CB_SELECTION_ALU;
											CF_INPUT <= ALU_CF;
											SF_INPUT <= ALU_SF;
											OF_INPUT <= ALU_OF;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											IP_INC <= '1'; --32bit instruction
											-- wait for memory to be ready and continue in next step
										when "10" => -- M <= R
											AB_SELECT <= AB_SELECTION_EXM;
											ALB2_SELECT <= CB_OUTPUT(6 downto 4);
											ALB1_SELECT <= AB_SELECTION_MEM;
											ALU_SELECT <= ALU_SELECTION_SUB;
											AB_SELECT <= AB_SELECTION_EXM;
											CB_SELECT <= CB_SELECTION_ALU;
											CF_INPUT <= ALU_CF;
											SF_INPUT <= ALU_SF;
											OF_INPUT <= ALU_OF;
											ZF_INPUT <= ALU_ZF;
											IP_INC <= '1'; --32bit instruction
											MEM_LOAD <= '1';
										when "11" => -- R <= A
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= AB_SELECTION_EXM;
											ALU_SELECT <= ALU_SELECTION_SUB;
											CB_SELECT <= CB_SELECTION_ALU;
											CF_INPUT <= ALU_CF;
											SF_INPUT <= ALU_SF;
											OF_INPUT <= ALU_OF;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											IP_INC <= '1'; --32bit instruction
											when others => NULL;
									end case;
								when INSTRUCTION_AND =>
									case CB_OUTPUT(9 downto 8) is
										when "00" => -- R <= R
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= CB_OUTPUT(2 downto 0);
											ALU_SELECT <= ALU_SELECTION_AND;
											CB_SELECT <= CB_SELECTION_ALU;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
										when "01" => -- R <= M
											AB_SELECT <= AB_SELECTION_EXM;
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= AB_SELECTION_MEM;
											ALU_SELECT <= ALU_SELECTION_AND;
											CB_SELECT <= CB_SELECTION_ALU;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											IP_INC <= '1'; --32bit instruction
											-- wait for memory to be ready and continue in next step
										when "10" => -- M <= R
											AB_SELECT <= AB_SELECTION_EXM;
											ALB2_SELECT <= CB_OUTPUT(6 downto 4);
											ALB1_SELECT <= AB_SELECTION_MEM;
											ALU_SELECT <= ALU_SELECTION_AND;
											AB_SELECT <= AB_SELECTION_EXM;
											CB_SELECT <= CB_SELECTION_ALU;
											ZF_INPUT <= ALU_ZF;
											IP_INC <= '1'; --32bit instruction
											MEM_LOAD <= '1';
										when "11" => -- R <= A
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= AB_SELECTION_EXM;
											ALU_SELECT <= ALU_SELECTION_AND;
											CB_SELECT <= CB_SELECTION_ALU;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											IP_INC <= '1'; --32bit instruction
											when others => NULL;
									end case;
								when INSTRUCTION_OR =>
									case CB_OUTPUT(9 downto 8) is
										when "00" => -- R <= R
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= CB_OUTPUT(2 downto 0);
											ALU_SELECT <= ALU_SELECTION_OR;
											CB_SELECT <= CB_SELECTION_ALU;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
										when "01" => -- R <= M
											AB_SELECT <= AB_SELECTION_EXM;
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= AB_SELECTION_MEM;
											ALU_SELECT <= ALU_SELECTION_OR;
											CB_SELECT <= CB_SELECTION_ALU;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											IP_INC <= '1'; --32bit instruction
											-- wait for memory to be ready and continue in next step
										when "10" => -- M <= R
											AB_SELECT <= AB_SELECTION_EXM;
											ALB2_SELECT <= CB_OUTPUT(6 downto 4);
											ALB1_SELECT <= AB_SELECTION_MEM;
											ALU_SELECT <= ALU_SELECTION_OR;
											AB_SELECT <= AB_SELECTION_EXM;
											CB_SELECT <= CB_SELECTION_ALU;
											ZF_INPUT <= ALU_ZF;
											IP_INC <= '1'; --32bit instruction
											MEM_LOAD <= '1';
										when "11" => -- R <= A
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= AB_SELECTION_EXM;
											ALU_SELECT <= ALU_SELECTION_OR;
											CB_SELECT <= CB_SELECTION_ALU;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											IP_INC <= '1'; --32bit instruction
											when others => NULL;
									end case;
								when INSTRUCTION_XOR =>
									case CB_OUTPUT(9 downto 8) is
										when "00" => -- R <= R
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= CB_OUTPUT(2 downto 0);
											ALU_SELECT <= ALU_SELECTION_XOR;
											CB_SELECT <= CB_SELECTION_ALU;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											ZF_INPUT <= ALU_ZF;
										when "01" => -- R <= M
											AB_SELECT <= AB_SELECTION_EXM;
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= AB_SELECTION_MEM;
											ALU_SELECT <= ALU_SELECTION_ADD;
											CB_SELECT <= CB_SELECTION_ALU;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											IP_INC <= '1'; --32bit instruction
											-- wait for memory to be ready and continue in next step
										when "10" => -- M <= R
											AB_SELECT <= AB_SELECTION_EXM;
											ALB2_SELECT <= CB_OUTPUT(6 downto 4);
											ALB1_SELECT <= AB_SELECTION_MEM;
											ALU_SELECT <= ALU_SELECTION_XOR;
											AB_SELECT <= AB_SELECTION_EXM;
											CB_SELECT <= CB_SELECTION_ALU;
											ZF_INPUT <= ALU_ZF;
											IP_INC <= '1'; --32bit instruction
											MEM_LOAD <= '1';
										when "11" => -- R <= A
											ALB1_SELECT <= CB_OUTPUT(6 downto 4);
											ALB2_SELECT <= AB_SELECTION_EXM;
											ALU_SELECT <= ALU_SELECTION_XOR;
											CB_SELECT <= CB_SELECTION_ALU;
											ZF_INPUT <= ALU_ZF;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_IP => IP_LOAD <= '1';
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											IP_INC <= '1'; --32bit instruction
											when others => NULL;
									end case;
							-------------ALU-FUNCTIONS--------
							-------------FLAG-SET-UNSET-------
								when INSTRUCTION_SET =>
									case CB_OUTPUT(6 downto 4) is
										when FL_SELECTION_ZF => ZF_INPUT <= '1';
										when FL_SELECTION_SF => SF_INPUT <= '1';
										when FL_SELECTION_OF => OF_INPUT <= '1';
										when FL_SELECTION_CF => CF_INPUT <= '1';
										when FL_SELECTION_IEN=> IEN_INPUT <= '1';
										when FL_SELECTION_IRF=> IEN_INPUT <= '1';
										when FL_SELECTION_ORF=> IEN_INPUT <= '1';
										when others => NULL;
									end case;
								when INSTRUCTION_USET =>
									case CB_OUTPUT(6 downto 4) is
										when FL_SELECTION_ZF => ZF_INPUT <= '0';
										when FL_SELECTION_SF => SF_INPUT <= '0';
										when FL_SELECTION_OF => OF_INPUT <= '0';
										when FL_SELECTION_CF => CF_INPUT <= '0';
										when FL_SELECTION_IEN=> IEN_INPUT <= '0';
										when FL_SELECTION_IRF=> IEN_INPUT <= '0';
										when FL_SELECTION_ORF=> IEN_INPUT <= '0';
										when others => NULL;
									end case;
							-------------FLAG-SET-UNSET-------
							-------------REGISTER-MOD---------
								when INSTRUCTION_INC =>
									case CB_OUTPUT(7 downto 4) is
										when CB_SELECTION_GR1 => 
											GR1_INC <= '1';
											if GR1_OUTPUT = "1111111111111111" then
												ZF_INPUT <= '1';
												OF_INPUT <= '0';
												SF_INPUT <= '0';
												CF_INPUT <= '1';
											else if GR1_OUTPUT = "0111111111111111" then
												ZF_INPUT <= '0';
												OF_INPUT <= '1';
												SF_INPUT <= '1';
												CF_INPUT <= '0';
											else if GR1_OUTPUT(15) = '1' then
												ZF_INPUT <= '0';
												OF_INPUT <= '0';
												SF_INPUT <= '1';
												CF_INPUT <= '0';
												else if GR1_OUTPUT(15) = '0' then
												ZF_INPUT <= '0';
												OF_INPUT <= '0';
												SF_INPUT <= '0';
												CF_INPUT <= '0';
											end if;
											end if;
											end if;
											end if;
										when CB_SELECTION_GR2 =>
											GR2_INC <= '1';
											if GR2_OUTPUT = "1111111111111111" then
												ZF_INPUT <= '1';
												OF_INPUT <= '0';
												SF_INPUT <= '0';
												CF_INPUT <= '1';
											else if GR2_OUTPUT = "0111111111111111" then
												ZF_INPUT <= '0';
												OF_INPUT <= '1';
												SF_INPUT <= '1';
												CF_INPUT <= '0';
											else if GR2_OUTPUT(15) = '1' then
												ZF_INPUT <= '0';
												OF_INPUT <= '0';
												SF_INPUT <= '1';
												CF_INPUT <= '0';										
											else if GR2_OUTPUT(15) = '0' then
												ZF_INPUT <= '0';
												OF_INPUT <= '0';
												SF_INPUT <= '0';
												CF_INPUT <= '0';
											end if;
											end if;
											end if;
											end if;
										when CB_SELECTION_GR3 =>
											GR3_INC <= '1';
											if GR3_OUTPUT = "1111111111111111" then
												ZF_INPUT <= '1';
												OF_INPUT <= '0';
												SF_INPUT <= '0';
												CF_INPUT <= '1';
											else if GR3_OUTPUT = "0111111111111111" then
												ZF_INPUT <= '0';
												OF_INPUT <= '1';
												SF_INPUT <= '1';
												CF_INPUT <= '0';
											else if GR3_OUTPUT(15) = '1' then
												ZF_INPUT <= '0';
												OF_INPUT <= '0';
												SF_INPUT <= '1';
												CF_INPUT <= '0';										
											else if GR3_OUTPUT(15) = '0' then
												ZF_INPUT <= '0';
												OF_INPUT <= '0';
												SF_INPUT <= '0';
												CF_INPUT <= '0';
											end if;
											end if;
											end if;
											end if;
										when CB_SELECTION_GR4 =>
											GR4_INC <= '1';
											if GR4_OUTPUT = "1111111111111111" then
												ZF_INPUT <= '1';
												OF_INPUT <= '0';
												SF_INPUT <= '0';
												CF_INPUT <= '1';
											else if GR4_OUTPUT = "0111111111111111" then
												ZF_INPUT <= '0';
												OF_INPUT <= '1';
												SF_INPUT <= '1';
												CF_INPUT <= '0';
											else if GR4_OUTPUT(15) = '1' then
												ZF_INPUT <= '0';
												OF_INPUT <= '0';
												SF_INPUT <= '1';
												CF_INPUT <= '0';										
											else if GR4_OUTPUT(15) = '0' then
												ZF_INPUT <= '0';
												OF_INPUT <= '0';
												SF_INPUT <= '0';
												CF_INPUT <= '0';
											end if;
											end if;
											end if;
											end if;
										when others => NULL;	
									end case;
								when INSTRUCTION_SHL =>
									case CB_OUTPUT(7 downto 4) is
										when CB_SELECTION_GR1 => 
											GR1_SHL <= '1';
											CF_INPUT <= GR1_OUTPUT(15);
											SF_INPUT <= GR1_OUTPUT(14);
											OF_INPUT <= GR1_OUTPUT(15) xor GR1_OUTPUT(14);
											if GR1_OUTPUT(14 downto 0) = "000000000000000" then
												ZF_INPUT <= '1';
											else
												ZF_INPUT <= '0';
											end if;
										when CB_SELECTION_GR2 =>
											GR2_SHL <= '1';
											CF_INPUT <= GR2_OUTPUT(15);
											SF_INPUT <= GR2_OUTPUT(14);
											OF_INPUT <= GR2_OUTPUT(15) xor GR2_OUTPUT(14);
											if GR2_OUTPUT(14 downto 0) = "000000000000000" then
												ZF_INPUT <= '1';
											else
												ZF_INPUT <= '0';
											end if;
										when CB_SELECTION_GR3 =>
											GR3_SHL <= '1';
											CF_INPUT <= GR3_OUTPUT(15);
											SF_INPUT <= GR3_OUTPUT(14);
											OF_INPUT <= GR3_OUTPUT(15) xor GR3_OUTPUT(14);
											if GR3_OUTPUT(14 downto 0) = "000000000000000" then
												ZF_INPUT <= '1';
											else
												ZF_INPUT <= '0';
											end if;
										when CB_SELECTION_GR4 =>
											GR4_SHL <= '1';
											CF_INPUT <= GR4_OUTPUT(15);
											SF_INPUT <= GR4_OUTPUT(14);
											OF_INPUT <= GR4_OUTPUT(15) xor GR4_OUTPUT(14);
											if GR4_OUTPUT(14 downto 0) = "000000000000000" then
												ZF_INPUT <= '1';
											else
												ZF_INPUT <= '0';
											end if;
										when others => NULL;	
									end case;
								when INSTRUCTION_SHR =>
									case CB_OUTPUT(7 downto 4) is
										when CB_SELECTION_GR1 => 
											GR1_SHR <= '1';
											CF_INPUT <= GR1_OUTPUT(0);
											SF_INPUT <= '0';
											OF_INPUT <= GR1_OUTPUT(15) xor '0';
											if GR1_OUTPUT(15 downto 1) = "000000000000000" then
												ZF_INPUT <= '1';
											else
												ZF_INPUT <= '0';
											end if;
										when CB_SELECTION_GR2 =>
											GR2_SHR <= '1';
											CF_INPUT <= GR2_OUTPUT(0);
											SF_INPUT <= '0';
											OF_INPUT <= GR2_OUTPUT(15) xor '0';
											if GR2_OUTPUT(15 downto 1) = "000000000000000" then
												ZF_INPUT <= '1';
											else
												ZF_INPUT <= '0';
											end if;
										when CB_SELECTION_GR3 =>
											GR3_SHR <= '1';
											CF_INPUT <= GR3_OUTPUT(0);
											SF_INPUT <= '0';
											OF_INPUT <= GR3_OUTPUT(15) xor '0';
											if GR3_OUTPUT(15 downto 1) = "000000000000000" then
												ZF_INPUT <= '1';
											else
												ZF_INPUT <= '0';
											end if;
										when CB_SELECTION_GR4 =>
											GR4_SHR <= '1';
											CF_INPUT <= GR4_OUTPUT(0);
											SF_INPUT <= '0';
											OF_INPUT <= GR4_OUTPUT(15) xor '0';
											if GR4_OUTPUT(15 downto 1) = "000000000000000" then
												ZF_INPUT <= '1';
											else
												ZF_INPUT <= '0';
											end if;
										when others => NULL;	
									end case;
							-------------REGISTER-MOD---------
							-------------I/O------------------
								when INSTRUCTION_INPUT =>
									case CB_OUTPUT(9 downto 8) is
										when "00" => -- Register
											CB_SELECT <= CB_SELECTION_DATA;
											case CB_OUTPUT(7 downto 4) is
												when CB_SELECTION_GR1 => GR1_LOAD <= '1';
												when CB_SELECTION_GR2 => GR2_LOAD <= '1';
												when CB_SELECTION_GR3 => GR3_LOAD <= '1';
												when CB_SELECTION_GR4 => GR4_LOAD <= '1';
												when others => NULL;	
											end case;
											IRF_INPUT <= '0';
										when others => NULL;
									end case;
								when INSTRUCTION_OUTPUT =>
									case CB_OUTPUT(9 downto 8) is
										when "00" => -- Register
											CB_SELECT <= CB_OUTPUT(7 downto 4);
											DATA_OUTPUT <= CB_OUTPUT(7 downto 0);
											if ORF_OUTPUT <= '1' then
												DATA_OUTPUT_ENABLE <= '1';
											end if;
										when others => NULL;
									end case;
							-------------I/O------------------
								when others => NULL;
							end case;
							STEPCOUNTER <= STEPCOUNTER + 1;
						end if;
					when 1 =>
--===============================================================
-- STEP 2
--===============================================================
					-- DO NOT USE MEMORY OUTPUTS HERE --
					-- DO NOT USE ADDRESS BUS HERE --
						case IR_OUTPUT(15 downto 10) is
						-------------MOV------------------
							when INSTRUCTION_MOV =>
								if IR_OUTPUT(9 downto 8) = "01" then
									case IR_OUTPUT(7 downto 4) is
										when CB_SELECTION_IP => IP_LOAD <= '1';
										when CB_SELECTION_GR1 => GR1_LOAD <= '1';
										when CB_SELECTION_GR2 => GR2_LOAD <= '1';
										when CB_SELECTION_GR3 => GR3_LOAD <= '1';
										when CB_SELECTION_GR4 => GR4_LOAD <= '1';
										when others => NULL;
									end case;
								end if;
						-------------MOV-------------------
						-------------JUMPS-----------------
							when INSTRUCTION_JMP => 
								if IR_OUTPUT(9 downto 8) = "01" or IR_OUTPUT(9 downto 8) = "11" then
									IP_LOAD <= '1';
								end if;
							when INSTRUCTION_JZ =>
								if (IR_OUTPUT(9 downto 8) = "01" or IR_OUTPUT(9 downto 8) = "11") and ZF_OUTPUT = '1' then
									IP_LOAD <= '1';
								end if;
							when INSTRUCTION_JNZ =>
								if (IR_OUTPUT(9 downto 8) = "01" or IR_OUTPUT(9 downto 8) = "11") and ZF_OUTPUT = '0' then
									IP_LOAD <= '1';
								end if;
							when INSTRUCTION_JC =>
								if (IR_OUTPUT(9 downto 8) = "01" or IR_OUTPUT(9 downto 8) = "11") and CF_OUTPUT = '1' then
									IP_LOAD <= '1';
								end if;
							when INSTRUCTION_JNC =>
								if (IR_OUTPUT(9 downto 8) = "01" or IR_OUTPUT(9 downto 8) = "11") and CF_OUTPUT = '0' then
									IP_LOAD <= '1';
								end if;
							when INSTRUCTION_JO =>
								if (IR_OUTPUT(9 downto 8) = "01" or IR_OUTPUT(9 downto 8) = "11") and OF_OUTPUT = '1' then
									IP_LOAD <= '1';
								end if;
							when INSTRUCTION_JNO =>
								if (IR_OUTPUT(9 downto 8) = "01" or IR_OUTPUT(9 downto 8) = "11") and OF_OUTPUT = '0' then
									IP_LOAD <= '1';
								end if;
							when INSTRUCTION_JS  =>
								if (IR_OUTPUT(9 downto 8) = "01" or IR_OUTPUT(9 downto 8) = "11") and SF_OUTPUT = '1' then
									IP_LOAD <= '1';
								end if;
							when INSTRUCTION_JNS =>
								if (IR_OUTPUT(9 downto 8) = "01" or IR_OUTPUT(9 downto 8) = "11") and ZF_OUTPUT = '0' then
									IP_LOAD <= '1';
								end if;
							when INSTRUCTION_JIR =>
								if (IR_OUTPUT(9 downto 8) = "01" or IR_OUTPUT(9 downto 8) = "11") and IRF_OUTPUT = '1' then
									IP_LOAD <= '1';
								end if;
							when INSTRUCTION_JNIR =>
								if (IR_OUTPUT(9 downto 8) = "01" or IR_OUTPUT(9 downto 8) = "11") and IRF_OUTPUT = '0' then
									IP_LOAD <= '1';
								end if;
							when INSTRUCTION_JOR =>
								if (IR_OUTPUT(9 downto 8) = "01" or IR_OUTPUT(9 downto 8) = "11") and ORF_OUTPUT = '0' then
									IP_LOAD <= '1';
								end if;
							when INSTRUCTION_JNOR =>
								if (IR_OUTPUT(9 downto 8) = "01" or IR_OUTPUT(9 downto 8) = "11") and ORF_OUTPUT = '1' then
									IP_LOAD <= '1';
								end if;
						-------------JUMPS-----------------
						-------------CALL------------------
							-- CALL FUNCTION IS BROKEN DUE TO IT REQUIRING MORE THAN 2 
							-- PULSES TO COMPLETE THE INSTRUCTION
--							when INSTRUCTION_CALL & "00" | INSTRUCTION_CALL & "01" | INSTRUCTION_CALL & "10" =>
--								--IP is updated to new value, CB is free and old IP is saved in TR
--								AB_SELECT <= AB_SELECTION_RETNADDR;
--								CB_SELECT <= CB_SELECTION_TR;
--								MEM_LOAD <= '1';
--							when INSTRUCTION_RETN & "11" =>
--								ALB1_SELECT <= AB_SELECTION_IP;
--								ALB2_SELECT <= AB_SELECTION_TWO;
--								CB_SELECT <= CB_SELECTION_ALU;
--								IP_LOAD <= '1';
						-------------CALL------------------
						-------------ALU-FUNCTIONS--------
							when INSTRUCTION_ADD | INSTRUCTION_SUB =>
								CF_INPUT <= ALU_CF;
								ZF_INPUT <= ALU_ZF;
								OF_INPUT <= ALU_OF;
								SF_INPUT <= ALU_SF;
							when INSTRUCTION_XOR | INSTRUCTION_OR | INSTRUCTION_AND =>
								ZF_INPUT <= ALU_ZF;
						-------------ALU-FUNCTIONS--------
							when others => NULL;
						end case;
						AB_SELECT <= AB_SELECTION_IP;
						STEPCOUNTER <= STEPCOUNTER + 1;
					when 2 =>
						CB_SELECT <= CB_SELECTION_MEM; 
						IP_INC <= '1';
						IR_LOAD <= '1'; -- Load memory into IR TODO: Move this outside of process
						STEPCOUNTER <= 0;
					when 3 =>
						--special interrupt state
						--set IP to value stored in 0x1
						--Memory is already ready at address 0x0
						CB_SELECT <= CB_SELECTION_EXM;
						IP_LOAD <= '1';
						AB_SELECT <= AB_SELECTION_IP;
						STEPCOUNTER <= 2;
					when others => STEPCOUNTER <= 0;
				end case;
			end if;
		end process;
	
end Behavioral;

