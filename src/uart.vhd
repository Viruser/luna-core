------------------------------------------------------------------------------------------
--
--  \ /   LUNA CORE
--  ( )   UART.VHD -- UART Interface for I/O with processor
--  / \   Copyright (C) 2015 Arman Hajishafieha                   
--
--    This file is part of LUNA CORE.
--
--    LUNA CORE is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    LUNA CORE is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with LUNA CORE.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity UART is
	port( UART_RX_VECTOR: out std_logic_vector(7 downto 0);
			UART_TX_VECTOR: in std_logic_vector(7 downto 0);
			UART_TX_ENABLE: in std_logic;
			UART_TX_READY: out std_logic ;
			UART_RX_READY: out std_logic;
			-- physical ports
			UART_RX_SERIAL: in std_logic;
			UART_TX_SERIAL: out std_logic;
			UART_CLK: in std_logic);
end UART;

architecture Behavioral of UART is	
	type STATE_T is (idle, busy);
	signal UART_ACTIVATE: std_logic;
	signal UART_RX_STATE: STATE_T := idle;
	signal UART_TX_STATE: STATE_T := idle;
	signal UART_RX_BIT_COUNTER: integer range 0 to 7 := 0;
	signal UART_TX_BIT_COUNTER: integer range 0 to 8 := 0;
	signal UART_CLK_COUNTER: integer := 0;
begin
	process(UART_CLK)

		begin
			if UART_CLK'event and UART_CLK = '1' then
				if UART_ACTIVATE = '1' then
					if UART_ACTIVATE <= '1' then -- we have clock pulse
						case UART_RX_STATE is
							when idle =>
								if UART_RX_SERIAL = '0' then 
									UART_RX_STATE <= busy; 
									UART_RX_READY <= '0';
								else
									UART_RX_READY <= '1';
								end if;
							when busy => NULL;
								if UART_RX_BIT_COUNTER = 7 then -- got 8 bits of data
									UART_RX_VECTOR(UART_RX_BIT_COUNTER) <= UART_RX_SERIAL;			
									UART_RX_STATE <= idle;
									UART_RX_READY <= '1';
									UART_RX_BIT_COUNTER <= 0;
								else
									UART_RX_VECTOR(UART_RX_BIT_COUNTER) <= UART_RX_SERIAL;			
									UART_RX_READY <= '0';
									UART_RX_BIT_COUNTER <= UART_RX_BIT_COUNTER + 1;
								end if;
						end case;
						case UART_TX_STATE is
							when idle =>
								UART_TX_READY <= '1';
								if UART_TX_ENABLE = '1' then
									UART_TX_READY <= '0';
									UART_TX_STATE <= busy;
									UART_TX_SERIAL <= '0'; -- send start bit
								else
									UART_TX_SERIAL <= '1'; -- send stop bit
									UART_TX_STATE <= idle;
								end if;
							when busy =>
								if UART_TX_BIT_COUNTER = 8 then
									UART_TX_SERIAL <= '1'; -- send 8 bits of data. send stop bit
									UART_TX_READY <= '1';
									UART_TX_BIT_COUNTER <= 0;
								else
									UART_TX_SERIAL <= UART_TX_VECTOR(UART_TX_BIT_COUNTER);
									UART_TX_READY <= '0';
									UART_TX_BIT_COUNTER <= UART_TX_BIT_COUNTER + 1;
								end if;
						end case;
					end if;
				end if;
				-- Create a 115200hz clock
				UART_CLK_COUNTER <= UART_CLK_COUNTER + 115200;
				if UART_CLK_COUNTER >= 50000000 then
					UART_CLK_COUNTER <= UART_CLK_COUNTER - 49884800; --50MHz - 11200
					UART_ACTIVATE <= '1';
				else
					UART_ACTIVATE <= '0';
				end if;
			end if;
	end process;

	process(UART_ACTIVATE)
		begin
		end process;
end Behavioral;

