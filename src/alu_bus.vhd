------------------------------------------------------------------------------------------
--
--  \ /   LUNA CORE
--  ( )   ALU_BUS.VHD -- BUS used to select registers as ALU inputs
--  / \   Copyright (C) 2015 Arman Hajishafieha                   
--
--    This file is part of LUNA CORE.
--
--    LUNA CORE is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    LUNA CORE is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with LUNA CORE.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.CONSTANTS.ALL;

entity ALU_BUS is
	port(ALB_INPUT_GR1: in std_logic_vector(15 downto 0);
	     ALB_INPUT_GR2: in std_logic_vector(15 downto 0);
		  ALB_INPUT_GR3: in std_logic_vector(15 downto 0);
		  ALB_INPUT_GR4: in std_logic_vector(15 downto 0);
		  ALB_INPUT_IP: in std_logic_vector(15 downto 0);
		  ALB_INPUT_EXM: in std_logic_vector(15 downto 0);
		  ALB_INPUT_MEM: in std_logic_vector(15 downto 0);
		  ALB_SELECT: in std_logic_vector(2 downto 0);
		  ALB_OUTPUT: out std_logic_vector(15 downto 0));
end ALU_BUS;

architecture Behavioral of ALU_BUS is

begin

	with ALB_SELECT select
		ALB_OUTPUT <= ALB_INPUT_GR1 when AB_SELECTION_GR1, ALB_INPUT_GR2 when AB_SELECTION_GR2,
						 ALB_INPUT_GR3 when AB_SELECTION_GR3, ALB_INPUT_GR4 when AB_SELECTION_GR4,
						 ALB_INPUT_IP when AB_SELECTION_IP, ALB_INPUT_EXM when AB_SELECTION_EXM,
						 ALB_INPUT_MEM when AB_SELECTION_MEM,
						 "0000000000000010" when AB_SELECTION_TWO,
						 "0000000000000000" when others;
end Behavioral;

