------------------------------------------------------------------------------------------
--
--  \ /   LUNA CORE
--  ( )   FULLADDER_16.VHD -- 16bit Carry look ahead full adder
--  / \   Copyright (C) 2015 Arman Hajishafieha                   
--
--    This file is part of LUNA CORE.
--
--    LUNA CORE is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    LUNA CORE is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with LUNA CORE.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FULLADDER_16 is
	port(FA_IN1: in std_logic_vector(15 downto 0);
		  FA_IN2: in std_logic_vector(15 downto 0);
		  FA_CIN: in std_logic;
		  FA_SUM: out std_logic_vector(15 downto 0);
		  FA_COUT: out std_logic);
end FULLADDER_16;



architecture Behavioral of FULLADDER_16 is
signal P, G: std_logic_vector(15 downto 0);
signal C: std_logic_vector(16 downto 0);

begin
	P <= FA_IN1 xor FA_IN2;
	G <= FA_IN1 and FA_IN2;
	C(0) <= FA_CIN;
	
	gen: for i in 0 to 15 generate
		C(i+1) <= G(i) or (P(i) and C(i));
		FA_SUM(i) <= C(i) xor P(i);
	end generate;
	
	FA_COUT <= C(16);
end Behavioral;

