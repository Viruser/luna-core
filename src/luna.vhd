------------------------------------------------------------------------------------------
--
--  \ /   LUNA CORE
--  ( )   LUNA.VHD -- Top block that connects cpu core(s), RAM, ROM and other IO modules
--  / \   Copyright (C) 2015 Arman Hajishafieha
--
--    This file is part of LUNA CORE.
--
--    LUNA CORE is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    LUNA CORE is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with LUNA CORE.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.CONSTANTS.ALL;

entity LUNA is
	port( GCLK: in std_logic;
			GRX: in std_logic;
			GTX: out std_logic);
end LUNA;

architecture Behavioral of LUNA is
component CORE is
	port (CLK: in std_logic;
			MEM_OUTPUT: in std_logic_vector(15 downto 0);
			MEM_EXTRAOUT: in std_logic_vector(15 downto 0);
			MEM_LOAD: out std_logic;
			MEM_INPUT: out std_logic_vector(15 downto 0);
			MEM_ADDRESS: out std_logic_vector(15 downto 0);
			DATA_OUTPUT: out std_logic_vector(7 downto 0);
			DATA_INPUT: in std_logic_vector(7 downto 0);
			DATA_INPUT_READY: in std_logic;
			DATA_OUTPUT_READY: in std_logic;
			DATA_OUTPUT_ENABLE: out std_logic;
			CEF_INPUT: in std_logic);
end component;

component MEMORY_16384x16 is
	port (MEM_OUTPUT: out std_logic_vector(15 downto 0);
			MEM_EXTRAOUT: out std_logic_vector(15 downto 0);
			MEM_ADDRESS: in std_logic_vector (15 downto 0);
			MEM_INPUT: in std_logic_vector (15 downto 0);
			MEM_LOAD: in std_logic := '0';
			MEM_CLK: in std_logic);
end component;	

component UART is
	port( UART_RX_VECTOR: out std_logic_vector(7 downto 0);
			UART_TX_VECTOR: in std_logic_vector(7 downto 0);
			UART_TX_ENABLE: in std_logic;
			UART_TX_READY: out std_logic;
			UART_RX_READY: out std_logic;
			-- physical ports
			UART_RX_SERIAL: in std_logic;
			UART_TX_SERIAL: out std_logic;
			UART_CLK: in std_logic);
end component;
component ROM_256x16 is
	port(ROM_ADDRESS: in std_logic_vector(7 downto 0);
		  ROM_OUTPUT: out std_logic_vector(15 downto 0);
		  ROM_CLK: in std_logic);
end component;

	signal MEM_OUTPUT, MEM_EXTRAOUT, MEM_INPUT, ROM_OUTPUT: std_logic_vector(15 downto 0);
	signal CPU0_MEM_ADDRESS, CPU0_MEM_INPUT, SOC_MEM_INPUT: std_logic_vector(15 downto 0);
	signal CPU0_MEM_LOAD, MEM_LOAD, CPU0_CEF_INPUT: std_logic := '0';
	signal MEM_ADDRESS: std_logic_vector(15 downto 0) := "0000000000000000";
	signal SOC_MEM_ADDRESS, ROM_ADDRESS, UART_RX_VECTOR, UART_TX_VECTOR: std_logic_vector(7 downto 0) := "00000000";
	signal UART_RX_READY, UART_TX_ENABLE, UART_TX_READY: std_logic;
	signal STARTUP_COUNTER: std_logic_vector(8 downto 0) := "000000000";

begin
	CPU: CORE port map(MEM_OUTPUT => MEM_OUTPUT, MEM_EXTRAOUT => MEM_EXTRAOUT, MEM_LOAD => CPU0_MEM_LOAD,
							 MEM_ADDRESS => CPU0_MEM_ADDRESS, MEM_INPUT => CPU0_MEM_INPUT, DATA_OUTPUT_ENABLE => UART_TX_ENABLE,
							 DATA_INPUT => UART_RX_VECTOR, DATA_OUTPUT => UART_TX_VECTOR, DATA_INPUT_READY => UART_RX_READY,
							 DATA_OUTPUT_READY => UART_TX_READY, CEF_INPUT => CPU0_CEF_INPUT, CLK => GCLK);
							 
	RAM: MEMORY_16384x16 port map(MEM_OUTPUT => MEM_OUTPUT, MEM_EXTRAOUT => MEM_EXTRAOUT, MEM_LOAD => MEM_LOAD,
											MEM_ADDRESS => MEM_ADDRESS, MEM_INPUT => MEM_INPUT, MEM_CLK => GCLK);
	
	ROM: ROM_256x16 port map(ROM_ADDRESS => ROM_ADDRESS, ROM_OUTPUT => ROM_OUTPUT, ROM_CLK => GCLK);
	
	IO0: UART port map(UART_RX_VECTOR => UART_RX_VECTOR, UART_TX_VECTOR => UART_TX_VECTOR,
							UART_TX_READY => UART_TX_READY,UART_RX_READY => UART_RX_READY,
							UART_TX_ENABLE => UART_TX_ENABLE, UART_RX_SERIAL => GRX, 
							UART_TX_SERIAL => GTX, UART_CLK => GCLK);
							
	-- We did not create a IO bus because there is only one IO device present in this configuration
	with STARTUP_COUNTER(8) select
		MEM_ADDRESS <= CPU0_MEM_ADDRESS when '1', "00000000" & SOC_MEM_ADDRESS when others;
	with STARTUP_COUNTER(8) select
		MEM_LOAD <= CPU0_MEM_LOAD when '1', '1' when others;
	with STARTUP_COUNTER(8) select
		MEM_INPUT <= CPU0_MEM_INPUT when '1', SOC_MEM_INPUT when others;
	SOC_MEM_INPUT <= ROM_OUTPUT;
	
	-- Copy Rom data to RAM before enabling core processor.
	process(GCLK)
		begin
		if GCLK'event and GCLK = '1'  and STARTUP_COUNTER(8) = '0' then
			if STARTUP_COUNTER(7 downto 0) = X"FF" then
				CPU0_CEF_INPUT <= '1';
			else
				CPU0_CEF_INPUT <= '0';
			end if;
			SOC_MEM_ADDRESS <= std_logic_vector(unsigned(STARTUP_COUNTER(7 downto 0)));
			ROM_ADDRESS <= std_logic_vector(unsigned(STARTUP_COUNTER(7 downto 0)) + 1);
			STARTUP_COUNTER <= std_logic_vector(unsigned(STARTUP_COUNTER) + 1);
		end if;
	end process;
end Behavioral;

