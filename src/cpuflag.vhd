------------------------------------------------------------------------------------------
--
--  \ /   LUNA CORE
--  ( )   CPUFLAG.VHD -- The register that holds cpu flags
--  / \   Copyright (C) 2015 Arman Hajishafieha
--
--    This file is part of LUNA CORE.
--
--    LUNA CORE is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    LUNA CORE is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with LUNA CORE.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity CPUFLAGS is
	port(CF_INPUT: in std_logic;
		  OF_INPUT: in std_logic;
		  ZF_INPUT: in std_logic;
		  SF_INPUT: in std_logic;
		  CEF_INPUT:in std_logic;
		  IEN_INPUT:in std_logic;
		  IRF_INPUT:in std_logic;
		  ORF_INPUT:in std_logic;
		  
		  CF_OUTPUT: out std_logic;
		  OF_OUTPUT: out std_logic;
		  ZF_OUTPUT: out std_logic;
		  SF_OUTPUT: out std_logic;
		  CEF_OUTPUT: out std_logic;
		  IEN_OUTPUT: out std_logic;
		  IRF_OUTPUT: out std_logic;
		  ORF_OUTPUT: out std_logic;
		  
		  FL_LOAD: in std_logic;
		  FL_CLK: in std_logic);
end CPUFLAGS;

architecture Behavioral of CPUFLAGS is

begin
	process(FL_CLK)
		begin
			if FL_CLK'event and FL_CLK = '1' and FL_LOAD = '1' then
				CF_OUTPUT <= CF_INPUT;
				OF_OUTPUT <= OF_INPUT;
				ZF_OUTPUT <= ZF_INPUT;
				SF_OUTPUT <= SF_INPUT;
				IEN_OUTPUT <= IEN_INPUT;
				CEF_OUTPUT <= CEF_INPUT;
				IRF_OUTPUT <= IRF_INPUT;
				ORF_OUTPUT <= ORF_INPUT;
			end if;
		end process;
end Behavioral;

