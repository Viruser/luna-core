------------------------------------------------------------------------------------------
--
--  \ /   LUNA CORE
--  ( )   COMMON_BUS.VHD -- Main BUS used to move data between ALU, registers and memory
--  / \   Copyright (C) 2015 Arman Hajishafieha                   
--
--    This file is part of LUNA CORE.
--
--    LUNA CORE is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    LUNA CORE is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with LUNA CORE.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.CONSTANTS.ALL;

entity COMMON_BUS is
	port(CB_INPUT_IP: in std_logic_vector(15 downto 0);
		  CB_INPUT_GR1: in std_logic_vector(15 downto 0);
		  CB_INPUT_GR2: in std_logic_vector(15 downto 0);
		  CB_INPUT_GR3: in std_logic_vector(15 downto 0);
		  CB_INPUT_GR4: in std_logic_vector(15 downto 0);
	      CB_INPUT_DATA: in std_logic_vector(7 downto 0);
		  CB_INPUT_MEM: in std_logic_vector(15 downto 0);
		  CB_INPUT_ALU: in std_logic_vector(15 downto 0);
		  CB_INPUT_EXM: in std_logic_vector(15 downto 0);
		  CB_INPUT_TR: in std_logic_vector(15 downto 0);
		  CB_SELECT: in std_logic_vector(3 downto 0);
		  CB_OUTPUT: out std_logic_vector(15 downto 0));
end COMMON_BUS;

architecture Behavioral of COMMON_BUS is
begin
	with CB_SELECT select
		CB_OUTPUT <= CB_INPUT_IP when CB_SELECTION_IP, CB_INPUT_GR1 when CB_SELECTION_GR1,
						 CB_INPUT_GR2 when CB_SELECTION_GR2,
						 CB_INPUT_GR3 when CB_SELECTION_GR3,
						 CB_INPUT_GR4 when CB_SELECTION_GR4,
						 (15 downto 8 => '0', 7 => CB_INPUT_DATA(7), 6 => CB_INPUT_DATA(6),
						  5 => CB_INPUT_DATA(5), 4 => CB_INPUT_DATA(4), 3 => CB_INPUT_DATA(3),
						  2 => CB_INPUT_DATA(2), 1 => CB_INPUT_DATA(1), 0 => CB_INPUT_DATA(0)) when CB_SELECTION_DATA,
						 CB_INPUT_EXM when CB_SELECTION_EXM, CB_INPUT_TR when CB_SELECTION_TR,
						 CB_INPUT_ALU when CB_SELECTION_ALU, CB_INPUT_MEM when CB_SELECTION_MEM,
						 "0000000000000000" when others;
end Behavioral;

