------------------------------------------------------------------------------------------
--
--  \ /   LUNA CORE
--  ( )   ALU.VHD -- Arithmetic & logical Unit
--  / \   Copyright (C) 2015-2016 Arman Hajishafieha
--
--    This file is part of LUNA CORE.
--
--    LUNA CORE is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    LUNA CORE is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with LUNA CORE.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.CONSTANTS.ALL;

entity ARITHMETICLOGICALUNIT is
	port(ALU_INPUT_1: in std_logic_vector(15 downto 0);
		  ALU_INPUT_2: in std_logic_vector(15 downto 0);
		  ALU_SELECT:  in std_logic_vector(2 downto 0);
		  ALU_OUTPUT:  inout std_logic_vector(15 downto 0);
		  ALU_CF: out std_logic;
		  ALU_OF: out std_logic;
		  ALU_ZF: out std_logic;
		  ALU_SF: out std_logic);
end ARITHMETICLOGICALUNIT;


architecture Behavioral of ARITHMETICLOGICALUNIT is
component FULLADDER_16 is
	port(FA_IN1: in std_logic_vector(15 downto 0);
		  FA_IN2: in std_logic_vector(15 downto 0);
		  FA_CIN: in std_logic;
		  FA_SUM: out std_logic_vector(15 downto 0);
		  FA_COUT: out std_logic);
end component;

	signal FA_SUM, FA_IN2: std_logic_vector(15 downto 0);
	signal FA_COUT, FA_CIN: std_logic;
begin

	FA: FULLADDER_16 port map(FA_IN1 => ALU_INPUT_1, FA_IN2 => FA_IN2, FA_CIN => FA_CIN,
									  FA_SUM => FA_SUM, FA_COUT => FA_COUT);
									  
	FA_IN2 <= ALU_INPUT_2 xor "1111111111111111" when ALU_SELECT = ALU_SELECTION_SUB else ALU_INPUT_2;
	FA_CIN <= '1' when ALU_SELECT = ALU_SELECTION_SUB else '0';
	
	with ALU_SELECT select
		ALU_OUTPUT <= FA_SUM when ALU_SELECTION_ADD,
						  FA_SUM when ALU_SELECTION_SUB,
						  (ALU_INPUT_1 and ALU_INPUT_2) when ALU_SELECTION_AND,
						  (ALU_INPUT_1 or ALU_INPUT_2) when ALU_SELECTION_OR,
						  (ALU_INPUT_1 xor ALU_INPUT_2) when ALU_SELECTION_XOR,
						  (others => '0') when others;
	with ALU_SELECT select
		ALU_CF <= FA_COUT when ALU_SELECTION_ADD, FA_COUT when ALU_SELECTION_SUB, '0' when others;
		
	with ALU_SELECT select
		ALU_OF <= ALU_INPUT_1(15) xor ALU_INPUT_2(15) xor FA_COUT when ALU_SELECTION_ADD,
					 ALU_INPUT_1(15) xor ALU_INPUT_2(15) xor FA_COUT when ALU_SELECTION_SUB,
					 '0' when others;	
	with ALU_SELECT select
		ALU_CF <= FA_COUT when ALU_SELECTION_ADD, FA_COUT when ALU_SELECTION_SUB, '0' when others;
		
	with ALU_SELECT select
		ALU_SF <= FA_SUM(15) when ALU_SELECTION_ADD, FA_SUM(15) when ALU_SELECTION_SUB, '0' when others;

		ALU_ZF <= '1' when ALU_SELECT = ALU_SELECTION_ADD and FA_SUM = "0000000000000000" else
					 '1' when ALU_SELECT = ALU_SELECTION_SUB and FA_SUM = "0000000000000000" else
					 '1' when ALU_SELECT = ALU_SELECTION_AND and ALU_OUTPUT = "0000000000000000" else
					 '1' when ALU_SELECT = ALU_SELECTION_OR  and ALU_OUTPUT = "0000000000000000" else
					 '1' when ALU_SELECT = ALU_SELECTION_XOR and ALU_OUTPUT = "0000000000000000" else '0';

		
end Behavioral;

